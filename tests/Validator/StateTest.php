<?php

namespace ServiceCore\Validate\Test\Validator;

use PHPUnit\Framework\TestCase as Test;
use ServiceCore\Validate\Validator\State;

class StateTest extends Test
{
    /** @var State */
    private $validator;

    public function setUp(): void
    {
        $this->validator = new State();
    }

    public function testValidState(): void
    {
        self::assertTrue($this->validator->isValid('CO'));
    }

    public function testInvalidState(): void
    {
        self::assertFalse($this->validator->isValid('POO'));
    }

    public function testErrorMessage(): void
    {
        $this->validator->isValid('POO');

        $messages = $this->validator->getMessages();
        
        self::assertEquals(
            'The selected state POO does not appear to be a valid US state',
            \end($messages)
        );
    }
}
