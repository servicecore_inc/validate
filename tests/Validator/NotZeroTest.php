<?php

namespace ServiceCore\Validate\Test\Validator;

use PHPUnit\Framework\TestCase;
use ServiceCore\Validate\Validator\NotZero;

class NotZeroTest extends TestCase
{
    public function testIsValidReturnsFalseIfValueZero(): void
    {
        $validator = new NotZero();

        $this->assertFalse($validator->isValid(0));
    }

    public function testIsValidReturnsTrue(): void
    {
        $validator = new NotZero();

        $this->assertTrue($validator->isValid(1));
        $this->assertTrue($validator->isValid('abc'));
        $this->assertTrue($validator->isValid([]));
    }
}
