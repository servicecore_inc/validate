<?php

namespace ServiceCore\Validate\Test\Validator;

use PHPUnit\Framework\TestCase as Test;
use ServiceCore\Validate\Validator\Decimal;

/**
 * @group Decimal
 */
class DecimalTest extends Test
{
    public function testIsValidReturnsTrueIfBelowOrEqualMaxPrecision(): void
    {
        $validator = new Decimal(['maxPrecision' => 10]);

        self::assertTrue($validator->isValid(\str_repeat(1, 9)));
        self::assertTrue($validator->isValid(\str_repeat(1, 10)));
    }

    public function testIsValidReturnsTrueIfAboveOrEqualMinPrecision(): void
    {
        $validator = new Decimal(['minPrecision' => 2]);

        self::assertTrue($validator->isValid(222));
        self::assertTrue($validator->isValid(22));
    }

    public function testIsValidReturnsTrueIfBelowOrEqualMaxScale(): void
    {
        $validator = new Decimal(['maxScale' => 3]);

        self::assertTrue($validator->isValid(22.33));
        self::assertTrue($validator->isValid(22.333));
    }

    public function testIsValidReturnsTrueIfAboveOrEqualMinScale(): void
    {
        $validator = new Decimal(['minScale' => 1]);

        self::assertTrue($validator->isValid(22.3));
        self::assertTrue($validator->isValid(22.33));
    }

    public function testIsValidReturnsFalseIfNotNumeric(): void
    {
        $decimal = new Decimal();

        self::assertFalse($decimal->isValid('abc'));
    }

    public function testIsValidReturnsFalseIfInvalidMinScale(): void
    {
        $validator = new Decimal(['minScale' => 1]);

        self::assertFalse($validator->isValid(10000));
    }

    public function testIsValidReturnsFalseIfInvalidMaxScale(): void
    {
        $validator = new Decimal(['maxScale' => 3]);

        self::assertFalse($validator->isValid(10000.4444));
    }

    public function testIsValidReturnsFalseIfInvalidMinPrecision(): void
    {
        $validator = new Decimal(['minPrecision' => 5]);

        self::assertFalse($validator->isValid(1000));
    }

    public function testIsValidReturnsFalseIfInvalidMaxPrecision(): void
    {
        $validator = new Decimal(['maxPrecision' => 10]);

        self::assertFalse($validator->isValid(\str_repeat(1, 11)));
    }
}
