<?php

namespace ServiceCore\Validate\Test\Validator;

use Phpass\Strength;
use PHPUnit\Framework\TestCase as Test;
use ServiceCore\Validate\Validator\PasswordStrength;

class PasswordStrengthTest extends Test
{
    /** @var PasswordStrength */
    private $passwordTester;

    public function setUp(): void
    {
        $strengthTester       = new Strength();
        $this->passwordTester = new PasswordStrength($strengthTester);
    }

    public function testStrongPassword(): void
    {
        self::assertTrue($this->passwordTester->isValid('alskjdfoij20398j)(#J092j0293j4029j352038jf9*D98j'));
    }

    public function testWeakPassword(): void
    {
        $this->assertFalse($this->passwordTester->isValid('password'));
    }

    public function testWeakError(): void
    {
        $this->passwordTester->isValid('password');

        $messages = $this->passwordTester->getMessages();

        self::assertEquals(
            'The provided password does not meet the minimum strength requirements',
            \end($messages)
        );
    }
}
