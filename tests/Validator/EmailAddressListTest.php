<?php

namespace ServiceCore\Validate\Test\Validator;

use Laminas\Validator\EmailAddress;
use PHPUnit\Framework\TestCase;
use ServiceCore\Validate\Validator\EmailAddressList;

class EmailAddressListTest extends TestCase
{
    public function testIsValidReturnsFalseIfInvalidEmail(): void
    {
        $validator = new EmailAddressList(new EmailAddress());

        $this->assertFalse($validator->isValid(['invalid', 'invalid2']));
        $this->assertArrayHasKey(EmailAddressList::INVALID_LIST, $validator->getMessages());
        $this->assertStringContainsString('invalid', $validator->getMessages()[EmailAddressList::INVALID_LIST]);
        $this->assertStringContainsString('invalid2', $validator->getMessages()[EmailAddressList::INVALID_LIST]);
    }

    public function testIsValidReturnsTrueIfEmptyArray(): void
    {
        $validator = new EmailAddressList(new EmailAddress());

        $this->assertTrue($validator->isValid([]));
    }

    public function testIsValidReturnsTrueAndConvertsToArray(): void
    {
        $validator = new EmailAddressList(new EmailAddress());

        $this->assertTrue($validator->isValid('john@gmail.com'));
    }

    public function testIsValidReturnsFalseWithValidMix(): void
    {
        $validator = new EmailAddressList(new EmailAddress());
        $emails    = [
            'foo@bar.com',
            'bar@foo.com',
            'invalid email',
            '    another invalid email    '
        ];

        $this->assertFalse($validator->isValid($emails));

        $messages = \implode(',', $validator->getMessages());

        // Make sure we see the invalid items in the messages
        $this->assertGreaterThan(0, \strpos($messages, $emails[2]));
        $this->assertGreaterThan(0, \strpos($messages, $emails[3]));

        // ...and not the valid ones
        $this->assertFalse(\strpos($messages, $emails[0]));
        $this->assertFalse(\strpos($messages, $emails[1]));
    }

    public function testIsValidReturnsTrue(): void
    {
        $validator = new EmailAddressList(new EmailAddress());

        $this->assertTrue($validator->isValid(['john@gmail.com', 'bob@local.net']));
    }
}
