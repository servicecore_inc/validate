<?php

namespace ServiceCore\Validate\Test\Validator;

use PHPUnit\Framework\TestCase;
use ServiceCore\Validate\Validator\IsLongitude;

class IsLongitudeTest extends TestCase
{
    public function testIsValidReturnsFalseIfNotNumeric(): void
    {
        $validator = new IsLongitude();

        $this->assertFalse($validator->isValid('abc'));
        $this->assertArrayHasKey(IsLongitude::ITEM_NOT_A_NUMBER, $validator->getMessages());
    }

    public function testIsValidReturnsFalseIfOutOfBoundsUpper(): void
    {
        $validator = new IsLongitude();

        $this->assertFalse($validator->isValid(181));
        $this->assertArrayHasKey(IsLongitude::ITEM_OUT_OF_BOUNDS, $validator->getMessages());
    }

    public function testIsValidReturnsFalseIfOutOfBoundsLower(): void
    {
        $validator = new IsLongitude();

        $this->assertFalse($validator->isValid(-181));
        $this->assertArrayHasKey(IsLongitude::ITEM_OUT_OF_BOUNDS, $validator->getMessages());
    }

    public function testIsValidReturnsTrue(): void
    {
        $validator = new IsLongitude();

        $this->assertTrue($validator->isValid(95.54));
    }
}
