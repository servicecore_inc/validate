<?php

namespace ServiceCore\Validate\Test\Validator;

use PHPUnit\Framework\TestCase;
use ServiceCore\Validate\Validator\IsNull;

class IsNullTest extends TestCase
{
    public function testIsValidReturnsFalseIfNotNull(): void
    {
        $validator = new IsNull();

        $this->assertFalse($validator->isValid(1));
    }

    public function testIsValidReturnsTrueIfNull(): void
    {
        $validator = new IsNull();

        $this->assertTrue($validator->isValid(null));
    }
}
