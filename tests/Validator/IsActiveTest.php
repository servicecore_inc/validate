<?php

namespace ServiceCore\Validate\Test\Validator;

use DateTime;
use Doctrine\ORM\EntityManager;
use Exception;
use PHPUnit\Framework\TestCase;
use ServiceCore\Validate\Validator\IsActive;

class IsActiveTest extends TestCase
{
    public function testIsValidReturnsFalseIfEntityClassNotInOptions(): void
    {
        $entityManager = $this->createStub(EntityManager::class);
        $isActive      = new IsActive($entityManager);

        $this->assertFalse($isActive->isValid('fooBar'));
        $this->assertArrayHasKey(IsActive::ENTITY_CLASS_NOT_SET, $isActive->getMessages());
    }

    public function testIsValidReturnsFalseIfEntityNotFound(): void
    {
        $entityManager = $this->createMock(EntityManager::class);

        $entityManager->expects($this->once())
                      ->method('find')
                      ->willReturn(null);

        $isActive = new IsActive($entityManager, ['entity_class' => 'fooBar']);

        $this->assertFalse($isActive->isValid('fooBar'));
        $this->assertArrayHasKey(IsActive::ENTITY_MISSING, $isActive->getMessages());
    }

    public function testIsValidReturnsFalseIfEntityThrowsException(): void
    {
        $entityManager = $this->createMock(EntityManager::class);

        $entityManager->expects($this->once())
                      ->method('find')
                      ->willThrowException(new Exception());

        $isActive = new IsActive($entityManager, ['entity_class' => 'fooBar']);

        $this->assertFalse($isActive->isValid('fooBar'));
        $this->assertArrayHasKey(IsActive::ENTITY_MISSING, $isActive->getMessages());
    }

    public function testIsValidReturnsFalseIfEntityNotActive(): void
    {
        $entityManager = $this->createMock(EntityManager::class);
        $entity        = new class {
            public function getDeletedAt(): ?DateTime
            {
                return new DateTime();
            }
        };

        $entityManager->expects($this->once())
                      ->method('find')
                      ->willReturn($entity);

        $isActive = new IsActive($entityManager, ['entity_class' => 'fooBar']);

        $this->assertFalse($isActive->isValid('fooBar'));
        $this->assertArrayHasKey(IsActive::IS_DISABLED, $isActive->getMessages());
    }

    public function testIsValidReturnsTrue(): void
    {
        $entityManager = $this->createMock(EntityManager::class);
        $entity        = new class {
            public function getDeletedAt(): ?DateTime
            {
                return null;
            }
        };

        $entityManager->expects($this->once())
                      ->method('find')
                      ->willReturn($entity);

        $isActive = new IsActive($entityManager, ['entity_class' => 'fooBar']);

        $this->assertTrue($isActive->isValid('fooBar'));
    }
}
