<?php

namespace ServiceCore\Validate\Test\Validator;

use PHPUnit\Framework\TestCase as Test;
use ServiceCore\Validate\Validator\PostalCode;

class PostalCodeCATest extends Test
{
    /** @var PostalCode */
    private $validator;

    public function setUp(): void
    {
        $this->validator = new PostalCode(
            [
                'countryCodes' => [
                    PostalCode::COUNTRY_CODE_CANADA
                ]
            ]
        );
    }

    public function testValidPostalCode(): void
    {
        self::assertTrue($this->validator->isValid('S4H 9T7'));
    }

    public function testInvalidPostalCode(): void
    {
        self::assertFalse($this->validator->isValid('ABCD'));
    }

    public function testZipMessage(): void
    {
        $this->validator->isValid('ABCD');

        $messages = $this->validator->getMessages();

        self::assertEquals(
            'The given postal code ABCD does not appear to be valid',
            \end($messages)
        );
    }
}
