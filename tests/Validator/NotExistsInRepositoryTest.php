<?php

namespace ServiceCore\Validate\Test\Validator;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use ServiceCore\Validate\Validator\NotExistsInRepository;

class NotExistsInRepositoryTest extends TestCase
{
    public function testIsValidReturnsFalseIfItemIsArray(): void
    {
        $builder = $this->getMockBuilder(EntityManager::class);

        $builder->disableOriginalConstructor();

        $entityManager = $builder->getMock();
        $validator     = new NotExistsInRepository($entityManager);

        $this->assertFalse($validator->isValid(['abc']));
        $this->assertArrayHasKey(NotExistsInRepository::ITEM_IS_ARRAY, $validator->getMessages());
    }

    public function testIsValidThrowsExceptionIfMissingEntityNameOption(): void
    {
        $builder = $this->getMockBuilder(EntityManager::class);

        $builder->disableOriginalConstructor();

        $entityManager = $builder->getMock();
        $validator     = new NotExistsInRepository($entityManager);

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid option \'entity_name\'');

        $validator->isValid(1);
    }

    public function testIsValidThrowsExceptionIfMissingFieldNameOption(): void
    {
        $builder = $this->getMockBuilder(EntityManager::class);

        $builder->disableOriginalConstructor();

        $entityManager = $builder->getMock();
        $validator     = new NotExistsInRepository($entityManager);

        $validator->setOptions(
            [
                'entity_name' => 'user'
            ]
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid option \'field_name\'');

        $validator->isValid(1);
    }

    public function testIsValidReturnsFalseIfItemDoesExist(): void
    {
        $builder = $this->getMockBuilder(EntityManager::class);

        $builder->disableOriginalConstructor();

        $entityManager = $builder->getMock();
        $builder       = $this->getMockBuilder(EntityRepository::class);

        $builder->disableOriginalConstructor();

        $builder->setMethods(
            [
                'findBy'
            ]
        );

        $fieldName  = 'id';
        $repository = $builder->getMock();
        $params     = [$fieldName => 1];

        $repository->expects($this->once())
                   ->method('findBy')
                   ->with($params)
                   ->willReturn(['abc']);

        $entityManager->expects($this->once())
                      ->method('getRepository')
                      ->willReturn($repository);

        $validator = new NotExistsInRepository($entityManager);

        $validator->setOptions(
            [
                'entity_name' => 'user',
                'field_name'  => $fieldName
            ]
        );

        $this->assertFalse($validator->isValid(1));
        $this->assertArrayHasKey(NotExistsInRepository::ITEM_EXISTS, $validator->getMessages());
    }

    public function testIsValidReturnsTrue(): void
    {
        $builder = $this->getMockBuilder(EntityManager::class);

        $builder->disableOriginalConstructor();

        $entityManager = $builder->getMock();
        $builder       = $this->getMockBuilder(EntityRepository::class);

        $builder->disableOriginalConstructor();

        $builder->setMethods(
            [
                'findBy'
            ]
        );

        $fieldName  = 'id';
        $repository = $builder->getMock();
        $params     = [$fieldName => 1];

        $repository->expects($this->once())
                   ->method('findBy')
                   ->with($params)
                   ->willReturn(null);

        $entityManager->expects($this->once())
                      ->method('getRepository')
                      ->willReturn($repository);

        $validator = new NotExistsInRepository($entityManager);

        $validator->setOptions(
            [
                'entity_name' => 'user',
                'field_name'  => $fieldName
            ]
        );

        $this->assertTrue($validator->isValid(1));
    }
}
