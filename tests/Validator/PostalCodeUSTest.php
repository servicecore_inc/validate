<?php

namespace ServiceCore\Validate\Test\Validator;

use PHPUnit\Framework\TestCase as Test;
use ServiceCore\Validate\Validator\PostalCode;

class PostalCodeUSTest extends Test
{
    /** @var PostalCode */
    private $validator;

    public function setUp(): void
    {
        $this->validator = new PostalCode(
            [
                'countryCodes' => [
                    PostalCode::COUNTRY_CODE_US
                ]
            ]
        );
    }

    public function testValid5DigitZipCode(): void
    {
        self::assertTrue($this->validator->isValid(80120));
    }

    public function testValid10DigitZipCode(): void
    {
        self::assertTrue($this->validator->isValid('80120-1234'));
    }

    public function testInvalidZipCode(): void
    {
        self::assertFalse($this->validator->isValid('ABCD'));
    }

    public function testZipMessage(): void
    {
        $this->validator->isValid('ABCD');

        $messages = $this->validator->getMessages();

        self::assertEquals(
            'The given postal code ABCD does not appear to be valid',
            \end($messages)
        );
    }
}
