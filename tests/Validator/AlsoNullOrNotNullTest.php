<?php

namespace ServiceCore\Validate\Test\Validator;

use PHPUnit\Framework\TestCase;
use ServiceCore\Validate\Validator\AlsoNullOrNotNull;

class AlsoNullOrNotNullTest extends TestCase
{
    public function testIsValidReturnsFalseIfFieldNotNull(): void
    {
        $validator   = new AlsoNullOrNotNull();
        $expectedKey = 'foo';
        $data        = [
            $expectedKey => 123
        ];

        $validator->setOptions(
            [
                'field' => $expectedKey
            ]
        );

        $this->assertFalse($validator->isValid(null, $data));
        $this->assertArrayHasKey($validator::FIELD_NOT_NULL, $validator->getMessages());
    }

    public function testIsValidReturnsFalseIfFieldIsNull(): void
    {
        $validator   = new AlsoNullOrNotNull();
        $expectedKey = 'foo';
        $data        = [
            $expectedKey => 123
        ];

        $validator->setOptions(
            [
                'field' => $expectedKey . 'Bar'
            ]
        );

        $this->assertFalse($validator->isValid($expectedKey, $data));
        $this->assertArrayHasKey($validator::FIELD_NULL, $validator->getMessages());
    }

    public function testIsValidReturnsTrue(): void
    {
        $validator   = new AlsoNullOrNotNull();
        $expectedKey = 'foo';
        $data        = [
            $expectedKey => 123
        ];

        $validator->setOptions(
            [
                'field' => $expectedKey
            ]
        );

        $this->assertTrue($validator->isValid(1234, $data));
    }
}
