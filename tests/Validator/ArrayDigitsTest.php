<?php

namespace ServiceCore\Validate\Test\Validator;

use PHPUnit\Framework\TestCase as Test;
use ServiceCore\Validate\Validator\ArrayDigits;

/**
 * @group ArrayDigits
 */
class ArrayDigitsTest extends Test
{
    public function testIsValidReturnsTrueIfArrayOfIntegers(): void
    {
        $validator = new ArrayDigits();
        $values    = [0, 1, 2];

        self::assertTrue($validator->isValid($values));
    }

    public function testIsValidReturnsTrueIfInt(): void
    {
        $validator = new ArrayDigits();
        $values    = 1;

        self::assertTrue($validator->isValid($values));
    }

    public function testIsValidReturnsFalseIfArrayOfStrings(): void
    {
        $validator = new ArrayDigits();
        $values    = ['abc', 'def', 'ghi'];

        self::assertFalse($validator->isValid($values));
    }

    public function testIsValidReturnsFalseIfString(): void
    {
        $validator = new ArrayDigits();
        $values    = 'abc';

        self::assertFalse($validator->isValid($values));
    }

    public function testIsValidReturnsFalseIfEmptyString(): void
    {
        $validator = new ArrayDigits();
        $values    = '';

        self::assertFalse($validator->isValid($values));
    }

    public function testIsValidReturnsFalseIfEmptyArrayOfStrings(): void
    {
        $validator = new ArrayDigits();
        $values    = ['', '', ''];

        self::assertFalse($validator->isValid($values));
    }
}
