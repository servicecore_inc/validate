<?php

namespace ServiceCore\Validate\Test\Validator;

use libphonenumber\PhoneNumberUtil;
use PHPUnit\Framework\TestCase as Test;
use ReflectionClass;
use ServiceCore\Validate\Validator\Phone;

class PhoneTest extends Test
{
    /** @var  Phone */
    private $validator;

    /** @var array */
    private $data = [
        'valid'          => '(303) 867-5309',
        'validExtension' => '(303) 867 5309 x1234',
        'invalid'        => 'abc'
    ];

    public function setUp(): void
    {
        $this->validator = new Phone(PhoneNumberUtil::getInstance());
    }

    public function testConstructor(): void
    {
        $reflectedValidator = new ReflectionClass($this->validator);
        $phoneUtil          = $reflectedValidator->getProperty('phoneInstance');

        $phoneUtil->setAccessible(true);

        $this->assertInstanceOf(PhoneNumberUtil::class, $phoneUtil->getValue($this->validator));
    }

    public function testGetLocale(): void
    {
        $reflectedValidator = new ReflectionClass($this->validator);
        $locale             = $reflectedValidator->getProperty('locale');
        $localeValue        = 'en-US';

        $locale->setAccessible(true);
        $locale->setValue($this->validator, $localeValue);

        $this->assertEquals($localeValue, $this->validator->getLocale());
    }

    public function testSetLocale(): void
    {
        $reflectedValidator = new ReflectionClass($this->validator);
        $locale             = 'en-US';
        $localeProperty     = $reflectedValidator->getProperty('locale');

        $localeProperty->setAccessible(true);

        $this->assertSame($this->validator, $this->validator->setLocale($locale));
        $this->assertEquals($locale, $localeProperty->getValue($this->validator));
    }

    public function testIsValidReturnsTrue(): void
    {
        $validPhoneNumber = '+17632555555';

        $this->assertTrue($this->validator->isValid($validPhoneNumber));

        $validPhoneNumber = '+34972677500';

        $this->assertTrue($this->validator->isValid($validPhoneNumber));

        $validPhoneNumber = '+1613-991-3044';

        $this->assertTrue($this->validator->isValid($validPhoneNumber));

        $validPhoneNumber = '7632689319';

        $this->assertTrue($this->validator->isValid($validPhoneNumber));

        $this->assertTrue($this->validator->isValid($this->data['valid']));
    }

    public function testIsValidReturnsFalse(): void
    {
        $invalidPhoneNumber = 'foo';

        $this->assertFalse($this->validator->isValid($invalidPhoneNumber));

        $invalidPhoneNumber = '+11234567789';

        $this->assertFalse($this->validator->isValid($invalidPhoneNumber));

        $invalidPhoneNumber = '+01234567789';

        $this->assertFalse($this->validator->isValid($invalidPhoneNumber));

        $this->assertFalse($this->validator->isValid($this->data['invalid']));
    }

    public function testValidNumberWithExtension(): void
    {
        $this->assertTrue($this->validator->isValid($this->data['validExtension']));
    }
}
