<?php

namespace ServiceCore\Validate\Test\Validator;

use PHPUnit\Framework\TestCase as Test;
use ServiceCore\Validate\Validator\PostalCode;

class PostalCodePATest extends Test
{
    /** @var PostalCode */
    private $validator;

    public function setUp(): void
    {
        $this->validator = new PostalCode(
            [
                'countryCodes' => [
                    PostalCode::COUNTRY_CODE_PANAMA
                ]
            ]
        );
    }

    public function testValidZipCode(): void
    {
        self::assertTrue($this->validator->isValid('0101'));
    }

    public function testInvalidZipCode(): void
    {
        self::assertFalse($this->validator->isValid('ABCD'));
    }

    public function testZipMessage(): void
    {
        $this->validator->isValid('ABCD');

        $messages = $this->validator->getMessages();

        self::assertEquals(
            'The given postal code ABCD does not appear to be valid',
            \end($messages)
        );
    }
}
