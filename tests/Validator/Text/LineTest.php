<?php

namespace ServiceCore\Validate\Test\Validator\Text;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase as Test;
use ReflectionObject;
use ServiceCore\Validate\Validator\Text\Line;

/**
 * @group  text
 */
class LineTest extends Test
{
    public function testGetBlacklistReturnsArray(): void
    {
        $this->assertTrue(\is_array((new Line())->getBlacklist()));
    }

    public function testGetEncodingReturnsString(): void
    {
        $this->assertTrue(\is_string((new Line())->getEncoding()));
    }

    public function testGetMaxReturnsNullIfMaxDoesNotExist(): void
    {
        $this->assertNull((new Line())->getMax());
    }

    public function testGetMaxReturnsIntIfMaxDoesExist(): void
    {
        $max       = 1;
        $validator = new Line();
        $class     = new ReflectionObject($validator);

        $property = $class->getProperty('max');
        $property->setAccessible(true);
        $property->setValue($validator, $max);

        $this->assertEquals($max, $validator->getMax());
    }

    public function testGetMinReturnsNullIfMinDoesNotExist(): void
    {
        $this->assertNull((new Line())->getMin());
    }

    public function testGetMinReturnsIntIfMinDoesExist(): void
    {
        $min       = 1;
        $validator = new Line();
        $class     = new ReflectionObject($validator);

        $property = $class->getProperty('min');
        $property->setAccessible(true);
        $property->setValue($validator, $min);

        $this->assertEquals($min, $validator->getMin());
    }

    public function testIsValidReturnsFalseIfValueIsNotString(): void
    {
        $this->assertFalse((new Line())->isValid(1));
    }

    public function testIsValidReturnsFalseIfEncodingIsInvalid(): void
    {
        // get the three-byte euro symbol in UTF-8
        $string = \mb_convert_encoding('&#x20AC;', 'UTF-8', 'HTML-ENTITIES');

        // delete the last byte to create an invalid UTF-8 byte sequence
        $string = \substr($string, 0, 2);

        $this->assertFalse((new Line())->isValid($string));
    }

    public function testIsValidReturnsFalseIfValueIsTooShort(): void
    {
        $this->assertFalse((new Line())->setMin(999)->isValid('foo'));
    }

    public function testIsValidReturnsFalseIfValueIsTooLong(): void
    {
        $this->assertFalse((new Line())->setMax(1)->isValid('foo'));
    }

    public function testIsValidReturnsFalseIsValueOutsideRange(): void
    {
        $this->assertFalse((new Line())->setMin(1)->setMax(2)->isValid('foo'));
    }

    public function testIsValidReturnsFalseIfValueHasLessThanSymbol(): void
    {
        $this->assertFalse((new Line())->isValid('< foo'));
    }

    public function testIsValidReturnsFalseIfValueHasLineBreak(): void
    {
        $this->assertFalse((new Line())->isValid("\n foo"));
    }

    public function testIsValidReturnsTrueIfValueIsValid(): void
    {
        $this->assertTrue((new Line())->isValid('foo bar'));
    }

    public function testSetBlacklistReturnsSelf(): void
    {
        $blacklist = ['a'];
        $validator = new Line();
        $class     = new ReflectionObject($validator);

        $property = $class->getProperty('blacklist');
        $property->setAccessible(true);

        $this->assertSame($validator, $validator->setBlacklist($blacklist));
        $this->assertEquals($blacklist, $property->getValue($validator));
    }

    public function testSetEncodingThrowsInvalidArgumentExceptionIfEncodingIsInvalid(): void
    {
        $this->expectException(InvalidArgumentException::class);

        (new Line())->setEncoding('foo');
    }

    public function testSetEncodingReturnSelf(): void
    {
        $encoding  = \mb_internal_encoding();
        $validator = new Line();
        $class     = new ReflectionObject($validator);

        $property = $class->getProperty('encoding');
        $property->setAccessible(true);

        $this->assertSame($validator, $validator->setEncoding($encoding));
        $this->assertEquals($encoding, $property->getValue($validator));
    }

    public function testSetMaxThrowsInvalidArgumentExceptionIfMaxIsNegative(): void
    {
        $this->expectException(InvalidArgumentException::class);

        (new Line())->setMax(-1);
    }

    public function testSetMaxThrowsInvalidArgumentExceptionIfMaxIsBelowMin(): void
    {
        $this->expectException(InvalidArgumentException::class);

        (new Line())->setMin(10)->setMax(1);
    }

    public function testSetMaxReturnsSelfIfMaxIsValid(): void
    {
        $max       = 1;
        $validator = new Line();
        $class     = new ReflectionObject($validator);

        $property = $class->getProperty('max');
        $property->setAccessible(true);

        $this->assertSame($validator, $validator->setMax($max));
        $this->assertEquals($max, $property->getValue($validator));
    }

    public function testSetMinThrowsInvalidArgumentExceptionIfMinIsNegative(): void
    {
        $this->expectException(InvalidArgumentException::class);

        (new Line())->setMin(-1);
    }

    public function testSetMinThrowsInvalidArgumentExceptionIfMinIsAboveMax(): void
    {
        $this->expectException(InvalidArgumentException::class);

        (new Line())->setMax(1)->setMin(10);
    }

    public function testSetMinReturnsSelfIfMaxIsValid(): void
    {
        $min       = 1;
        $validator = new Line();
        $class     = new ReflectionObject($validator);

        $property = $class->getProperty('min');
        $property->setAccessible(true);

        $this->assertSame($validator, $validator->setMin($min));
        $this->assertEquals($min, $property->getValue($validator));
    }
}
