<?php

namespace ServiceCore\Validate\Test\Validator;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use ServiceCore\Validate\Validator\ArrayExistsInRepository;

class ArrayExistsInRepositoryTest extends TestCase
{
    public function testIsValidReturnsFalseIfNotArray(): void
    {
        $builder = $this->getMockBuilder(EntityManager::class);

        $builder->disableOriginalConstructor();

        $entityManager = $builder->getMock();
        $validator     = new ArrayExistsInRepository($entityManager);

        $this->assertFalse($validator->isValid('abc'));
        $this->assertArrayHasKey(ArrayExistsInRepository::NOT_ARRAY, $validator->getMessages());
    }

    public function testIsValidReturnsFalseIfItemTooLong(): void
    {
        $builder = $this->getMockBuilder(EntityManager::class);

        $builder->disableOriginalConstructor();

        $entityManager = $builder->getMock();
        $validator     = new ArrayExistsInRepository($entityManager);

        $validator->setOptions(
            [
                'max_length' => 5
            ]
        );

        $this->assertFalse($validator->isValid([1, 2, 3, 4, 5, 6]));
        $this->assertArrayHasKey(ArrayExistsInRepository::ITEM_TOO_LONG, $validator->getMessages());
    }

    public function testIsValidThrowsExceptionIfMissingEntityNameOption(): void
    {
        $builder = $this->getMockBuilder(EntityManager::class);

        $builder->disableOriginalConstructor();

        $entityManager = $builder->getMock();
        $validator     = new ArrayExistsInRepository($entityManager);

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid option \'entity_name\'');

        $validator->isValid([1]);
    }

    public function testIsValidThrowsExceptionIfMissingFieldNameOption(): void
    {
        $builder = $this->getMockBuilder(EntityManager::class);

        $builder->disableOriginalConstructor();

        $entityManager = $builder->getMock();
        $validator     = new ArrayExistsInRepository($entityManager);

        $validator->setOptions(
            [
                'entity_name' => 'abc'
            ]
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid option \'field_name\'');

        $validator->isValid([1]);
    }

    public function testIsValidReturnsFalseIfItemIsNull(): void
    {
        $builder = $this->getMockBuilder(EntityManager::class);

        $builder->disableOriginalConstructor();

        $builder->setMethods(
            [
                'getRepository'
            ]
        );

        $entityManager = $builder->getMock();
        $builder       = $this->getMockBuilder(EntityRepository::class);

        $builder->disableOriginalConstructor();

        $builder->setMethods(
            [
                'findBy'
            ]
        );

        $fieldName  = 'id';
        $repository = $builder->getMock();
        $params     = [$fieldName => 1];

        $repository->expects($this->once())
                   ->method('findBy')
                   ->with($params)
                   ->willReturn(null);

        $entityManager->expects($this->once())
                      ->method('getRepository')
                      ->willReturn($repository);

        $validator = new ArrayExistsInRepository($entityManager);

        $validator->setOptions(
            [
                'entity_name' => 'user',
                'field_name'  => $fieldName
            ]
        );

        $this->assertFalse($validator->isValid([1, 2]));
        $this->assertArrayHasKey(ArrayExistsInRepository::ITEM_NOT_EXISTS, $validator->getMessages());
    }

    public function testIsValidReturnsFalseIfItemIsEmptyArray(): void
    {
        $builder = $this->getMockBuilder(EntityManager::class);

        $builder->disableOriginalConstructor();

        $builder->setMethods(
            [
                'getRepository'
            ]
        );

        $entityManager = $builder->getMock();
        $builder       = $this->getMockBuilder(EntityRepository::class);

        $builder->disableOriginalConstructor();

        $builder->setMethods(
            [
                'findBy'
            ]
        );

        $fieldName  = 'id';
        $repository = $builder->getMock();
        $params     = [$fieldName => 1];

        $repository->expects($this->once())
                   ->method('findBy')
                   ->with($params)
                   ->willReturn([]);

        $entityManager->expects($this->once())
                      ->method('getRepository')
                      ->willReturn($repository);

        $validator = new ArrayExistsInRepository($entityManager);

        $validator->setOptions(
            [
                'entity_name' => 'user',
                'field_name'  => $fieldName
            ]
        );

        $this->assertFalse($validator->isValid([1, 2]));
        $this->assertArrayHasKey(ArrayExistsInRepository::ITEM_NOT_EXISTS, $validator->getMessages());
    }

    public function testIsValidReturnsTrue(): void
    {
        $builder = $this->getMockBuilder(EntityManager::class);

        $builder->disableOriginalConstructor();

        $builder->setMethods(
            [
                'getRepository'
            ]
        );

        $entityManager = $builder->getMock();
        $builder       = $this->getMockBuilder(EntityRepository::class);

        $builder->disableOriginalConstructor();

        $builder->setMethods(
            [
                'findBy'
            ]
        );

        $fieldName  = 'id';
        $repository = $builder->getMock();
        $params     = [$fieldName => 1];

        $repository->expects($this->once())
                   ->method('findBy')
                   ->with($params)
                   ->willReturn(['abc']);

        $entityManager->expects($this->once())
                      ->method('getRepository')
                      ->willReturn($repository);

        $validator = new ArrayExistsInRepository($entityManager);

        $validator->setOptions(
            [
                'entity_name' => 'user',
                'field_name'  => $fieldName
            ]
        );

        $this->assertTrue($validator->isValid([1]));
    }
}
