<?php

namespace ServiceCore\Validate\Test\Validator;

use PHPUnit\Framework\TestCase;
use ServiceCore\Validate\Validator\IsLatitude;

class IsLatitudeTest extends TestCase
{
    public function testIsValidReturnsFalseIfNotNumeric(): void
    {
        $validator = new IsLatitude();

        $this->assertFalse($validator->isValid('abc'));
        $this->assertArrayHasKey(IsLatitude::ITEM_NOT_A_NUMBER, $validator->getMessages());
    }

    public function testIsValidReturnsFalseIfOutOfBoundsUpper(): void
    {
        $validator = new IsLatitude();

        $this->assertFalse($validator->isValid(91));
        $this->assertArrayHasKey(IsLatitude::ITEM_OUT_OF_BOUNDS, $validator->getMessages());
    }

    public function testIsValidReturnsFalseIfOutOfBoundsLower(): void
    {
        $validator = new IsLatitude();

        $this->assertFalse($validator->isValid(-91));
        $this->assertArrayHasKey(IsLatitude::ITEM_OUT_OF_BOUNDS, $validator->getMessages());
    }

    public function testIsValidReturnsTrue(): void
    {
        $validator = new IsLatitude();

        $this->assertTrue($validator->isValid(34.54));
    }
}
