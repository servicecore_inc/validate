<?php

namespace ServiceCore\Validate\Test\Validator;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use ServiceCore\Validate\Validator\AlsoNull;

class AlsoNullTest extends TestCase
{
    public function testIsValidThrowsExceptionIfInvalidOptionField(): void
    {
        $validator = new AlsoNull();
        $data      = [
            'abc' => 123
        ];

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid option \'field\'');

        $validator->isValid(\json_encode($data));
    }

    public function testIsValidReturnsFalseIfFieldIsNotAlsoNull(): void
    {
        $validator = new AlsoNull();
        $field     = 'abc';
        $data      = [
            $field => 123
        ];

        $validator->setOptions(['field' => $field]);

        $this->assertFalse($validator->isValid(\json_encode($data)));
        $this->assertArrayHasKey(AlsoNull::FIELD_NOT_NULL, $validator->getMessages());
    }

    public function testIsValidReturnsTrue(): void
    {
        $validator = new AlsoNull();
        $field     = 'abc';
        $data      = [
            $field => null
        ];

        $validator->setOptions(['field' => $field]);

        $this->assertTrue($validator->isValid(\json_encode($data)));
    }
}
