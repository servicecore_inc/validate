<?php

namespace ServiceCore\Validate\Test\Validator;

use PHPUnit\Framework\TestCase;
use ServiceCore\Validate\Validator\OneOrOtherNull;

class OneOrOtherNullTest extends TestCase
{
    public function testIsValidReturnsFalseIfValueAndFieldNotNull(): void
    {
        $validator   = new OneOrOtherNull();
        $expectedKey = 'foo';
        $data        = [
            $expectedKey => 123
        ];

        $validator->setOptions(
            [
                'field' => $expectedKey
            ]
        );

        $this->assertFalse($validator->isValid(1, $data));
        $this->assertArrayHasKey($validator::FIELD_NOT_NULL, $validator->getMessages());
    }

    public function testIsValidReturnsTrueIfValueNullAndFieldNotNull(): void
    {
        $validator   = new OneOrOtherNull();
        $expectedKey = 'foo';
        $data        = [
            $expectedKey => 123
        ];

        $validator->setOptions(
            [
                'field' => $expectedKey
            ]
        );

        $this->assertTrue($validator->isValid(null, $data));
    }

    public function testIsValidReturnsTrueIfValueNotNullAndFieldNull(): void
    {
        $validator   = new OneOrOtherNull();
        $expectedKey = 'foo';
        $data        = [
            $expectedKey => null
        ];

        $validator->setOptions(
            [
                'field' => $expectedKey
            ]
        );

        $this->assertTrue($validator->isValid(1, $data));
    }
}
