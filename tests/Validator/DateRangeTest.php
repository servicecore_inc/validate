<?php

namespace ServiceCore\Validate\Test\Validator;

use PHPUnit\Framework\TestCase;
use ServiceCore\Validate\Validator\DateRange;

class DateRangeTest extends TestCase
{
    public function testIsValidReturnsFalseIfNotWithinRange(): void
    {
        $validator = new DateRange();
        $range     = 2;
        $expected  = $range + 1;
        $date      = (new \DateTime("now + ${expected} days"))->format('Y-m-d');

        $validator->setOptions(
            [
                'range' => $range
            ]
        );

        $this->assertFalse($validator->isValid($date));
        $this->assertArrayHasKey($validator::NOT_WITHIN_RANGE, $validator->getMessages());
    }

    public function testIsValidReturnsTrueIfWithinRange(): void
    {
        $validator = new DateRange();
        $range     = 2;
        $expected  = $range - 1;
        $date      = (new \DateTime("now + ${expected} days"))->format('Y-m-d');

        $validator->setOptions(
            [
                'range' => $range
            ]
        );

        $this->assertTrue($validator->isValid($date));
    }
}
