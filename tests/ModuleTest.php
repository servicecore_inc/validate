<?php

namespace ServiceCore\Validate\Test;

use HaydenPierce\ClassFinder\ClassFinder;
use PHPUnit\Framework\TestCase;
use ServiceCore\Validate\Module;
use ServiceCore\Validate\Validator\AbstractEntityValidator;

class ModuleTest extends TestCase
{
    public function testGetConfigRegistersValidatorFactories(): void
    {
        $module = new Module();
        $config = $module->getConfig();

        $this->assertArrayHasKey('validators', $config);
        $this->assertArrayHasKey('factories', $config['validators']);
    }

    public function testAllValidatorsAreRegistered(): void
    {
        $module         = new Module();
        $assertion      = false;
        $config         = $module->getConfig();
        $classes        = ClassFinder::getClassesInNamespace('ServiceCore\Validate\Validator');
        $ignoredClasses = [
            AbstractEntityValidator::class
        ];

        foreach ($classes as $class) {
            if (\in_array($class, $ignoredClasses, true)) {
                continue;
            }

            $this->assertArrayHasKey($class, $config['validators']['factories']);

            if (!$assertion) {
                $assertion = true;
            }
        }

        // this ensures the assertion happened in the foreach
        // this should never be hit
        if (!$assertion) {
            $this->assertFalse(true);
        }
    }
}
