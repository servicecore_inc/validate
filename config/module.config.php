<?php

use Laminas\ServiceManager\Factory\InvokableFactory;
use ServiceCore\Validate\Constraint\Factory\LimitByAssociation as LimitByAssociationFactory;
use ServiceCore\Validate\Constraint\Factory\LimitByInput as LimitByInputFactory;
use ServiceCore\Validate\Constraint\Factory\LimitByParam as LimitByParamFactory;
use ServiceCore\Validate\Constraint\Factory\LimitByRouteEntity as LimitByRouteEntityFactory;
use ServiceCore\Validate\Constraint\Factory\LimitByValue as LimitByValueFactory;
use ServiceCore\Validate\Constraint\LimitByAssociation;
use ServiceCore\Validate\Constraint\LimitByInput;
use ServiceCore\Validate\Constraint\LimitByParam;
use ServiceCore\Validate\Constraint\LimitByRouteEntity;
use ServiceCore\Validate\Constraint\LimitByValue;
use ServiceCore\Validate\Validator\AlsoNull;
use ServiceCore\Validate\Validator\AlsoNullOrNotNull;
use ServiceCore\Validate\Validator\ArrayDigits;
use ServiceCore\Validate\Validator\ArrayExistsInRepository;
use ServiceCore\Validate\Validator\DateRange;
use ServiceCore\Validate\Validator\Decimal;
use ServiceCore\Validate\Validator\EmailAddressList;
use ServiceCore\Validate\Validator\ExistsInRepository;
use ServiceCore\Validate\Validator\Factory\EmailAddressList as EmailAddressListFactory;
use ServiceCore\Validate\Validator\Factory\EntityValidator as EntityValidatorFactory;
use ServiceCore\Validate\Validator\Factory\PasswordStrength as PasswordStrengthFactory;
use ServiceCore\Validate\Validator\IsActive;
use ServiceCore\Validate\Validator\IsLatitude;
use ServiceCore\Validate\Validator\IsLongitude;
use ServiceCore\Validate\Validator\IsNull;
use ServiceCore\Validate\Validator\NotExistsInRepository;
use ServiceCore\Validate\Validator\NotZero;
use ServiceCore\Validate\Validator\OneOrOtherNull;
use ServiceCore\Validate\Validator\PasswordStrength;
use ServiceCore\Validate\Validator\Phone;
use ServiceCore\Validate\Validator\PostalCode;
use ServiceCore\Validate\Validator\State;

return [
    'service_manager' => [
        'factories' => [
            LimitByRouteEntity::class => LimitByRouteEntityFactory::class,
            LimitByParam::class       => LimitByParamFactory::class,
            LimitByAssociation::class => LimitByAssociationFactory::class,
            LimitByValue::class       => LimitByValueFactory::class,
            LimitByInput::class       => LimitByInputFactory::class,
        ]
    ],
    'validators'      => [
        'factories' => [
            AlsoNull::class                => InvokableFactory::class,
            AlsoNullOrNotNull::class       => InvokableFactory::class,
            ArrayDigits::class             => InvokableFactory::class,
            ArrayExistsInRepository::class => EntityValidatorFactory::class,
            DateRange::class               => InvokableFactory::class,
            Decimal::class                 => InvokableFactory::class,
            EmailAddressList::class        => EmailAddressListFactory::class,
            ExistsInRepository::class      => EntityValidatorFactory::class,
            IsLatitude::class              => InvokableFactory::class,
            IsLongitude::class             => InvokableFactory::class,
            IsNull::class                  => InvokableFactory::class,
            NotExistsInRepository::class   => EntityValidatorFactory::class,
            NotZero::class                 => InvokableFactory::class,
            OneOrOtherNull::class          => InvokableFactory::class,
            PasswordStrength::class        => PasswordStrengthFactory::class,
            Phone::class                   => InvokableFactory::class,
            PostalCode::class              => InvokableFactory::class,
            State::class                   => InvokableFactory::class,
            IsActive::class                => EntityValidatorFactory::class
        ]
    ]
];
