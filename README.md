Custom Laminas Framework Validators
=====================

### Password Strength

Password strength is determined using the `rych\phpass` library's Strength checking class, and calculates a given string's entropy. This follows NIST password strength calculations and requirements. See:

[rych\phpass library](https://github.com/rchouinard/phpass)

[NIST Strength Publication](https://en.wikipedia.org/wiki/Password_strength#NIST_Special_Publication_800-63)

[Entropy Checking Tool](http://rumkin.com/tools/password/passchk.php)

###### Options

```php
[
    'minStrength' => 30 // integer from 1-100 representing the minimum entropy of a password. Default is 30.
]
```

###### Usage

```php
$strengthCalcualtor = new \Phpass\Strength();

$strengthValidator = new \ServiceCore\Validate\PasswordStrength($strenthCalculator);

$strengthValidator->isValid('password');
```

### State

From a list of US states, determines if the provided state is a valid US state or territory. Currently runs in `\ServiceCore\Validate\State::MODE_US`, but could easily be expanded to include other countires or territories.

###### Usage

```php
$stateValidator = new \ServiceCore\Validate\State();

$stateValidator->isValid('CO'); // true!

$stateValidator->isValid('TEST'); // false >:|
```

### Postal Code

Uses a simple Regex string to validate US postal codes.

###### Usage

```php
$zipValidator = new \ServiceCore\Validate\PostalCode();

$zipValidator->isValid(80120); // true

$zipValidator->isValid('80120-1234'); // true

$zipValidator->isValid('TEST'); // false
```

### Phone

The phone number validator uses `giggsey/libphonenumber-for-php` as its base. See:

[giggsey/libphonenumber-for-php](https://github.com/giggsey/libphonenumber-for-php)

###### Usage

```php
$phoneInstance = \libphonenumber\PhoneNumberUtil::getInstance();

$phoneValidator = new \ServiceCore\Validate\Phone($phoneInstance);

$phoneValidator->isValid('303 806 0948'); // true

$phoneValidator->isValid('abc'); // false
```

###### Options

The validator can be given a locale option to ensure it formats the source of the phone number correctly. The libphonenumber package will always make a "best guess", but if you can provide the locale, it'll be more accurate.

```php
$phoneValidator = new \ServiceCore\Validate\Phone($phoneInstance, ['locale' => 'GB']);
```

## Text

The text validator validates the encoding, length, and characters of a _line_ or _block_ of text. 

By default, a _line_ or _block_ MUST be UTF-8 encoded, and it MUST NOT include the greater than (">") and less than ("<") symbols to prevent _encoding attacks_ and _XSS attacks_, respectively. In addition, a _line_ MUST NOT include a carriage-return ("\r") or new-line character ("\n"). 

###### Usage

```php
// create a text block validator
$validator = new Block();

$validator->isValid("foo bar");      // returns true
$validator->isValid("foo \n bar");   // returns true
$validator->isValid("<h1>hi</h1>");  // returns false

// create a text line validator
$validator = new Line();

$validator->isValid("foo bar");      // returns true
$validator->isValid("foo \n bar");   // returns false (no line breaks!)
$validator->isValid("<h1>hi</h1>");  // returns false
```

###### Options

The text validators have four options: 

* `blacklist`, a list of forbidden characters or sub-strings (defaults to `["<", ">"]` for block validator and `["<", ">", "\r", "\n"]` for line validator)
* `encoding`, the string's required character encoding (defaults to "UTF-8")
* `min`, the string's minimum length (inclusive), and
* `max`, the string's maximum length (inclusive)

WARNING! Setting the validator's blacklisted characters via the `blacklist` option will overwrite the default characters! DO NOT OPEN A SECURITY VULNERABILITY BY ALLOWING THE GREATER THAN AND LESS THAN SYMBOLS UNLESS THEY ARE ABSOLUTELY NECESSARY!

```php
// a (dangerous!) fully configured example
$validator = new Line([
    'blacklist' => ['!'],    // this will allow "<" and ">" characters!
    'encoding'  => 'UTF-8',
    'min'       => 3, 
    'max'       => 12
]);

$validator->isValid('foo');              // returns true
$validator->isValid('<h1>foo</h1>');     // returns true (danger!)
$validator->isValid('<h1>hi!</h1>');     // returns false (invalid character)
$validator->isValid('hi');               // returns false (too short)
$validator->isValid('foo bar baz qux');  // returns false (too long)
```

## Version

### 0.1.8, May 11, 2016

* Added `Text\Line` and `Text\Block` validators
