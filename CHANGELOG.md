# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 3.0.5 - 2023-01-24

### Fixed

- AlsoNullOrNotNull did not work if the comparison field existed in the array but was null

## 3.0.4 - 2022-10-25

### Added

- IsActive validator

## 3.0.3 - 2022-05-11

### Added

- OneOrOtherNull validator

## 3.0.2 - 2021-07-12

### Added

- DateRange validator

### Fixed

- Old Zend references

## 3.0.1 - 2021-07-12

### Added

- AlsoNullOrNotNull validator
- Constraints!

## 3.0.0 - 2020-10-01

### Added

- Support for Laminas

### Removed

- Support for Zend

## 2.0.0 - 2019-10-07

### Added

- Nothing.

### Changed

- Module namespace.

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Nothing.

## 1.0.12 - 2018-10-22

### Added

- NotExistsInRepository validator and tests
- Array return type to getConfig method in module
- Module tests

### Changed

- Nothing.

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Nothing.

## 1.0.11 - 2018-10-20

### Added

- EmailAddressList valid mix test 

### Changed

- Nothing.

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Missing factory registration for EmailAddressList validator

## 1.0.10 - 2018-10-20

### Added

- Add environment aware delegation to ExistsInRepository and ArrayExistsInRepository 

### Changed

- Nothing.

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Nothing.

## 1.0.9 - 2018-10-20

### Added

- ArrayExistsInRepository validator
- EmailAddressList validator
- ExistsInRepository validator
- IsLatitude validator
- IsLongitude validator
- IsNull validator
- NotZero validator
- Validator tests
- Throw exception in AlsoNull validator if Request is not an instance of Laminas\Http

### Changed

- Nothing.

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- AlsoNull validator json_decodes $value now instead of the request content
- Potential undefined offset in AlsoNull validator

## 1.0.8 - 2018-10-18

### Added

- New validators
- Factory registration for validators

### Changed

- Nothing.

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Nothing.
