<?php

namespace ServiceCore\Validate\Validator;

use Laminas\Validator\AbstractValidator;
use Laminas\Validator\EmailAddress;

class EmailAddressList extends AbstractValidator
{
    public const INVALID_LIST = 'emailAddressListInvalid';

    /** @var array */
    protected $messageTemplates = [
        self::INVALID_LIST => 'Errors were found with one or more email addresses: %addressList%',
    ];

    /** @var array */
    protected $messageVariables = [
        'addressList' => 'addressList'
    ];

    /** @var string */
    protected $addressList = '';

    /** @var EmailAddress */
    private $singleAddressValidator;

    public function __construct(EmailAddress $singleAddressValidator, array $options = [])
    {
        $this->singleAddressValidator = $singleAddressValidator;

        parent::__construct($options);
    }

    public function isValid($value): bool
    {
        // If we're sent just a string, convert to array. Largely so we can apply the same validator to all endpoints
        if (!\is_array($value)) {
            $value = [$value];
        }

        $invalidAddresses = [];

        foreach ($value as $email) {
            if (!$this->singleAddressValidator->isValid($email)) {
                $singleMessages = \implode(', ', $this->singleAddressValidator->getMessages());
                $invalidAddresses[] = \sprintf('%s (%s)', $email, $singleMessages);
            }
        }

        if (\count($invalidAddresses) > 0) {
            $this->addressList = \implode(', ', $invalidAddresses);
            $this->error(self::INVALID_LIST);

            return false;
        }

        return true;
    }
}
