<?php

namespace ServiceCore\Validate\Validator;

use Laminas\Validator\Exception\InvalidArgumentException;

class ArrayExistsInRepository extends ExistsInRepository
{
    public const NOT_ARRAY       = 'notArray';
    public const ITEM_TOO_LONG   = 'itemTooLong';
    public const ITEM_NOT_EXISTS = 'itemNotExists';

    /** @var array */
    protected $messageTemplates = [
        self::NOT_ARRAY       => 'The value provided is not an array',
        self::ITEM_NOT_EXISTS => 'Could not find item %value%',
        self::ITEM_TOO_LONG   => 'Value can not contain more than %value% elements',
    ];

    public function isValid($values): bool
    {
        try {
            $maxItems = $this->getOption('max_length');
        } catch (InvalidArgumentException $e) {
            $maxItems = null;
        }

        if (\is_array($values)) {
            if ($maxItems && \count($values) > $maxItems) {
                $this->error(self::ITEM_TOO_LONG, $maxItems);

                return false;
            }
            foreach ($values as $value) {
                if (!parent::isValid($value)) {
                    $this->error(self::ITEM_NOT_EXISTS);

                    return false;
                }
            }
        } else {
            $this->error(self::NOT_ARRAY);

            return false;
        }

        return true;
    }
}
