<?php

namespace ServiceCore\Validate\Validator;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;
use Laminas\Validator\AbstractValidator;

class Phone extends AbstractValidator
{
    public const UNRECOGNIZED_PHONE = 'unrecognized_phone';

    /** @var PhoneNumberUtil */
    private $phoneInstance;

    /** @var string */
    private $locale = 'US';

    /** @var array */
    protected $messageTemplates = [
        self::UNRECOGNIZED_PHONE => '%value% does not appear to be a valid phone number'
    ];

    public function __construct($options = null)
    {
        parent::__construct($options);

        $this->phoneInstance = PhoneNumberUtil::getInstance();
    }

    public function isValid($value): bool
    {
        $this->setValue($value);

        try {
            $phone = $this->phoneInstance->parse($value, $this->locale);

            if (!$isValid = $this->phoneInstance->isValidNumber($phone)) {
                $this->error(self::UNRECOGNIZED_PHONE);
            }
        } catch (NumberParseException $e) {
            $this->error(self::UNRECOGNIZED_PHONE);

            return false;
        }

        return $isValid;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }
}
