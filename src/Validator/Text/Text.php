<?php

namespace ServiceCore\Validate\Validator\Text;

use InvalidArgumentException;
use Laminas\Validator\AbstractValidator;

/**
 * The base text validator
 *
 * I'll validate *lines* and *blocks* of text. 
 *
 * I have four options: 
 *
 *     blacklist
 *         An array of blacklisted characters or substrings. Defaults to ["<", ">"] 
 *         to prevent XSS scripting attacks. Keep in mind, setting this value in the
 *         options will *over-write* the default values.
 *
 *     encoding
 *         The string required encoding. Defaults to "UTF-8" to prevent invalid 
 *         encoding attacks.
 *
 *     min
 *         The minimum number of characters in the string (inclusive). Defaults to
 *         null (i.e., no minimum).
 *
 *     max
 *         The maximum number of characters in the string (inclusive). Defaults to 
 *         null (i.e., no maximum).
 */
abstract class Text extends AbstractValidator
{
    /**
     * @var  string  the error code for an invalid character
     */
    public const ERROR_CHARACTER = 'character';

    /**
     * @var  string  the error code for an encoding mis-match
     */
    public const ERROR_ENCODING = 'encoding';

    /**
     * @var  string  the error code for a string above the maximum length or below
     *    the minimum length
     */
    public const ERROR_LENGTH = 'length';

    /**
     * @var  string  the error code for a value that is not a string
     */
    public const ERROR_TYPE = 'type';

    /**
     * @var  string[]  an array of blacklisted characters (defaults to ["<", ">"])
     */
    protected $blacklist = ['<', '>'];

    /**
     * @var  string  the string's required character encoding
     */
    protected $encoding = 'UTF-8';

    /** 
     * @var  int  the string's maximum length (inclusive)
     */
    protected $max;

    /**
     * @var  int  the string's minimum length (inclusive)
     */
    protected $min;

    /**
     * @var  string[]  an array of *default* error messages indexed by error code;
     *     keep in mind, these messages MUST be defined here, however, they will be
     *     set to more specific messages in the isValid() method
     */
    protected $messageTemplates = [
        self::ERROR_CHARACTER => "The string includes an invalid character",
        self::ERROR_ENCODING  => "The string is not properly encoded",
        self::ERROR_LENGTH    => "The string is too long or too long",
        self::ERROR_TYPE      => "The string isn't actually a string"
    ];

    public function getBlacklist(): array
    {
        return $this->blacklist;
    }

    public function getEncoding(): string
    {
        return $this->encoding;
    }

    public function getMax(): ?int
    {
        return $this->max;
    }

    public function getMin(): ?int
    {
        return $this->min;
    }

    public function setBlacklist(array $blacklist): self
    {
        $this->blacklist = $blacklist;

        return $this;
    }

    public function setEncoding(string $encoding): self
    {
        // if $encoding is not valid, short-circuit
        if (!\in_array($encoding, \mb_list_encodings())) {
            throw new InvalidArgumentException(
                __METHOD__ . '() expects parameter one, encoding, to be a valid '
                    . "encoding name"
            );
        }

        $this->encoding = $encoding;

        return $this;
    }

    public function setMax(int $max): self
    {
        // if $max is negative, short-circuit
        if ($max < 0) {
            throw new InvalidArgumentException(
                __METHOD__ . '() expects parameter one, max, to be a positive '
                    . "integer or zero"
            );
        }

        // if min exists and $max is less than min, short-circuit
        if ($this->min !== null && $max < $this->min) {
            throw new InvalidArgumentException(
                __METHOD__. '() expects parameter one, max, to be greater than or '
                    . "equal to the min value, {$this->min}"
            );
        }

        $this->max = $max;

        return $this;
    }

    public function setMin(int $min): self
    {
        // if $min is negative, short-circuit
        if ($min < 0) {
            throw new InvalidArgumentException(
                __METHOD__ . '() expects parameter one, min, to be a positive '
                    . 'integer or zero'
            );
        }

        // if max exists and $min is greater than max, short-circuit
        if ($this->max !== null && $min > $this->max) {
            throw new InvalidArgumentException(
                __METHOD__ . '() expects parameter one, min, to be less than or '
                    . "equal to the max value, {$this->max}"
            );
        }

        $this->min = $min;

        return $this;
    }

    public function isValid($value): bool
    {
        return $this->isValidType($value) 
            && $this->isValidEncoding($value)
            && $this->isValidLength($value)
            && $this->isValidCharacters($value);
    }

    /**
     * Returns true if the string's encoding is valid
     *
     * @param   string  $string  the string to validate
     * @return  bool
     */
    private function isValidEncoding(string $string): bool
    {
        // if the encoding is invalid
        if (!\mb_check_encoding($string, $this->encoding)) {
            // set the message, raise the error, and short-circuit
            $this->setMessage(
                "The string must be '{$this->encoding}' encoded, '"
                    .  \mb_detect_encoding($string, \mb_detect_order(), true) . "' "
                    . 'detected',
                self::ERROR_ENCODING
            );
            $this->error(self::ERROR_ENCODING);

            return false;
        }

        return true;
    }

    /**
     * Returns true if the string's characters are valid
     *
     * @param   string  $string  the string to validate
     * @return  bool
     */
    private function isValidCharacters(string $string): bool
    {
        // loop through the black-listed characters
        foreach ($this->blacklist as $character) {
            // if a black-listed character exists in the string
            if (false !== ($i = \mb_strpos($string, $character, 0, $this->encoding))) {
                // set the message, raise the error, and short-circuit
                $this->setMessage(
                    "The string must not include the following characters: '"
                        . \implode("', '", $this->blacklist) . "'; '{$character}'"
                        . "found at index {$i}",
                    self::ERROR_CHARACTER
                );
                $this->error(self::ERROR_CHARACTER);

                return false;
            }
        }

        return true;
    }

    /**
     * Returns true if the string's length is valid
     *
     * @param  string  $string  the string to validate
     * @return  bool
     */
    private function isValidLength(string $string): bool
    {
        // get the string's length
        $length = \mb_strlen($string, $this->encoding);

        // if both, either, or neither are defined
        if ($this->min !== null && $this->max !== null) {
            // if both are defined and the length is outside either
            if ($length < $this->min || $length > $this->max) {
                // set the message, raise the error, and short-circuit
                $this->setMessage(
                    "The string must be between {$this->min} and {$this->max} "
                        . "characters (inclusive); {$length} characters given",
                    self::ERROR_LENGTH
                );
                $this->error(self::ERROR_LENGTH);

                return false;
            }
        } elseif ($this->min !== null) {
            // otherwise, if min is defined and length is too short
            if ($length < $this->min) {
                // set the message, raise the error, and short-circuit
                $this->setMessage(
                    "The string must be greater than or equal to {$this->min} "
                        . "characters; {$length} characters given",
                    self::ERROR_LENGTH
                );
                $this->error(self::ERROR_LENGTH);

                return false;
            }
        } elseif ($this->max !== null) {
            // otherwise, if max is defined and length it too long
            if ($length > $this->max) {
                // set the message, raise the error, and short-circuit
                $this->setMessage(
                    "The string must be less than or equal to {$this->max} "
                        . "characters; {$length} characters given",
                    self::ERROR_LENGTH
                );
                $this->error(self::ERROR_LENGTH);

                return false;
            }
        }

        return true;
    }

    private function isValidType($string): bool
    {
        // if $string is not actually a string
        if (!\is_string($string)) {
            // set the message, raise the error, and short-circuit
            $this->setMessage(
                'The value must be a string; ' . \gettype($string) . ' given',
                self::ERROR_TYPE
            );
            $this->error(self::ERROR_TYPE);

            return false;
        }

        return true;
    }
}
