<?php

namespace ServiceCore\Validate\Validator\Text;

class Line extends Text
{
    public function __construct(array $options = [])
    {
        // append newline characters to the blacklist
        $this->blacklist = \array_merge($this->blacklist, ["\r", "\n"]);

        parent::__construct($options);
    }
}
