<?php

namespace ServiceCore\Validate\Validator;

use DateTime;
use InvalidArgumentException;
use Throwable;

class IsActive extends AbstractEntityValidator
{
    public const ENTITY_CLASS_NOT_SET = 'entityClassNotSet';
    public const ENTITY_MISSING       = 'entityMissing';
    public const IS_DISABLED          = 'isDisabled';

    /** @var array */
    protected $messageTemplates = [
        self::ENTITY_CLASS_NOT_SET => 'Required option `entity_class` is not set',
        self::ENTITY_MISSING       => 'Entity does not exist',
        self::IS_DISABLED          => 'Referenced entity is disabled and cannot be assigned to new entities',
    ];

    public function isValid($value): bool
    {
        try {
            $entityClass = $this->getOption('entity_class');
        } catch (InvalidArgumentException $e) {
            $this->error(self::ENTITY_CLASS_NOT_SET);

            return false;
        }

        try {
            $entity = $this->entityManager->find($entityClass, $value);

            if ($entity === null) {
                $this->error(self::ENTITY_MISSING);

                return false;
            }
        } catch (Throwable $e) {
            $this->error(self::ENTITY_MISSING);

            return false;
        }

        if (\method_exists($entity, 'getDeletedAt') && $entity->{'getDeletedAt'}() instanceof DateTime) {
            $this->error(self::IS_DISABLED);

            return false;
        }

        return true;
    }
}
