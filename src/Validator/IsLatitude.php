<?php

namespace ServiceCore\Validate\Validator;

use Laminas\Validator\AbstractValidator;

class IsLatitude extends AbstractValidator
{
    public const ITEM_NOT_A_NUMBER  = 'itemNotANumber';
    public const ITEM_OUT_OF_BOUNDS = 'itemOutOfBounds';

    /** @var array */
    protected $messageTemplates = [
        self::ITEM_NOT_A_NUMBER  => 'Value is not a number',
        self::ITEM_OUT_OF_BOUNDS => 'Value is out of bounds for a latitude',
    ];

    public function isValid($value): bool
    {
        if (!\is_numeric($value)) {
            $this->error(self::ITEM_NOT_A_NUMBER);

            return false;
        }

        if ($value < -90 || $value > 90) {
            $this->error(self::ITEM_OUT_OF_BOUNDS);

            return false;
        }

        return true;
    }
}
