<?php

namespace ServiceCore\Validate\Validator;

use Laminas\Validator\AbstractValidator;

class IsLongitude extends AbstractValidator
{
    public const ITEM_NOT_A_NUMBER  = 'itemNotANumber';
    public const ITEM_OUT_OF_BOUNDS = 'itemOutOfBounds';

    /** @var array */
    protected $messageTemplates = [
        self::ITEM_NOT_A_NUMBER  => 'Value is not a number',
        self::ITEM_OUT_OF_BOUNDS => 'Value is out of bounds for a longitude',
    ];

    public function isValid($value): bool
    {
        if (!\is_numeric($value)) {
            $this->error(self::ITEM_NOT_A_NUMBER);

            return false;
        }

        if ($value < -180 || $value > 180) {
            $this->error(self::ITEM_OUT_OF_BOUNDS);

            return false;
        }

        return true;
    }
}
