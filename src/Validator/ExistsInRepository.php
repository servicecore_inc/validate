<?php

namespace ServiceCore\Validate\Validator;

class ExistsInRepository extends AbstractEntityValidator
{
    public const ITEM_NOT_EXISTS = 'itemNotExists';
    public const ITEM_IS_ARRAY   = 'itemIsArray';

    /** @var array */
    protected $messageTemplates = [
        self::ITEM_NOT_EXISTS => 'The item %value% was not found',
        self::ITEM_IS_ARRAY   => 'The value passed in must not be an array'
    ];

    public function isValid($value): bool
    {
        if (\is_array($value)) {
            $this->error(self::ITEM_IS_ARRAY);

            return false;
        }

        $entity = $this->findEntity($value);

        if ($entity === null) {
            $this->error(self::ITEM_NOT_EXISTS);

            return false;
        }

        if (\count($entity) === 0) {
            $this->error(self::ITEM_NOT_EXISTS);

            return false;
        }

        return true;
    }
}
