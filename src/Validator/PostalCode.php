<?php

namespace ServiceCore\Validate\Validator;

use RuntimeException;
use Laminas\Validator\AbstractValidator;

class PostalCode extends AbstractValidator
{
    public const COUNTRY_CODE_US     = 'US';
    public const COUNTRY_CODE_CANADA = 'CA';
    public const COUNTRY_CODE_PANAMA = 'PA';

    public const INVALID_POSTAL_CODE = 'invalidPostalCode';

    /** @var array */
    protected $messageTemplates = [
        self::INVALID_POSTAL_CODE => 'The given postal code %value% does not appear to be valid'
    ];

    /** @var array */
    protected static $postCodeRegex = [
        self::COUNTRY_CODE_US     => '/^\d{5}(?:[-\s]?\d{4})?$/',
        self::COUNTRY_CODE_CANADA => '/^([a-zA-Z]\d[a-zA-Z])\ {0,1}(\d[a-zA-Z]\d)$/',
        self::COUNTRY_CODE_PANAMA => '/^\d{4}$/',
    ];

    /** @var array */
    private $countryCodes = [];

    public function __construct(array $options = [])
    {
        if (\array_key_exists('countryCodes', $options)) {
            if (\is_array($options['countryCodes'])) {
                foreach ($options['countryCodes'] as $countryCode) {
                    if (\array_key_exists($countryCode, self::$postCodeRegex)) {
                        $this->countryCodes[] = $countryCode;
                    }
                }
            } else {
                if (\array_key_exists($options['countryCodes'], self::$postCodeRegex)) {
                    $this->countryCodes[] = $options['countryCodes'];
                }
            }
        } else {
            throw new RuntimeException('Missing required `countryCodes` option');
        }

        parent::__construct($options);
    }

    public function isValid($value): bool
    {
        $this->setValue($value);

        foreach ($this->countryCodes as $countryCode) {
            if (\preg_match(self::$postCodeRegex[$countryCode], $value) === 1) {
                return true;
            }
        }

        $this->error(self::INVALID_POSTAL_CODE);

        return false;
    }
}
