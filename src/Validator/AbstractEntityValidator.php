<?php

namespace ServiceCore\Validate\Validator;

use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Laminas\Validator\AbstractValidator as BaseAbstractValidator;

abstract class AbstractEntityValidator extends BaseAbstractValidator
{
    protected EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager, $options = null)
    {
        $this->entityManager = $entityManager;

        parent::__construct($options);
    }

    abstract public function isValid($value): bool;

    protected function findEntity($value): ?array
    {
        $this->setValue($value);

        $results = null;

        if ($this->validateOptions()) {
            $repository = $this->entityManager->getRepository($this->getOption('entity_name'));
            $results    = $repository->findBy([$this->getOption('field_name') => $value]);
        }

        return $results;
    }

    private function validateOptions(): bool
    {
        if (!$this->getOption('entity_name') || !$this->getOption('field_name')) {
            throw new InvalidArgumentException(
                \sprintf('%s expects a entity_name and field_name key in the config array', __CLASS__)
            );
        }

        return true;
    }
}
