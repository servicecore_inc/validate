<?php

namespace ServiceCore\Validate\Validator;

use Phpass\Strength;
use Laminas\Validator\AbstractValidator;

class PasswordStrength extends AbstractValidator
{
    public const WEAK_PASSWORD = 'weakPassword';

    /** @var Strength */
    private $strengthTester;

    /** @var int */
    private $minStrength = 20;

    /** @var array */
    protected $messageTemplates = [
        self::WEAK_PASSWORD => 'The provided password does not meet the minimum strength requirements'
    ];

    public function __construct(Strength $strengthTester, array $options = null)
    {
        parent::__construct($options);

        $this->strengthTester = $strengthTester;
    }

    public function isValid($value): bool
    {
        if ($this->strengthTester->calculate($value) < $this->minStrength) {
            $this->error(self::WEAK_PASSWORD);
            return false;
        }

        return true;
    }
}
