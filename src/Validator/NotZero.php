<?php

namespace ServiceCore\Validate\Validator;

use Laminas\Validator\AbstractValidator;

class NotZero extends AbstractValidator
{
    public const ZERO = 'zero';

    /** @var array */
    protected $messageTemplates = [
        self::ZERO => 'Value must not be 0',
    ];

    public function isValid($value): bool
    {
        $isValid = true;

        if ($value === 0) {
            $this->error(self::ZERO);
            $isValid = false;
        }

        return $isValid;
    }
}
