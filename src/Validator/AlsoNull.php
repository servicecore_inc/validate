<?php

namespace ServiceCore\Validate\Validator;

use Laminas\Validator\AbstractValidator;

class AlsoNull extends AbstractValidator
{
    public const NOT_JSON                = 'notJson';
    public const FIELD_NOT_NULL          = 'fieldNotNull';
    public const NOT_JSON_VERBIAGE       = 'Data must be passed as a valid JSON string';
    public const FIELD_NOT_NULL_VERBIAGE = 'Field \'%value%\' must also be null';

    protected $messageTemplates = [
        self::NOT_JSON       => self::NOT_JSON_VERBIAGE,
        self::FIELD_NOT_NULL => self::FIELD_NOT_NULL_VERBIAGE
    ];

    public function isValid($value): bool
    {
        $data = \json_decode($value, true);

        if (\json_last_error() !== \JSON_ERROR_NONE) {
            $this->error(self::NOT_JSON);

            return false;
        }

        $field = $this->getOption('field');

        if (!\array_key_exists($field, $data) || $data[$field] !== null) {
            $this->error(self::FIELD_NOT_NULL, $field);

            return false;
        }

        return true;
    }
}
