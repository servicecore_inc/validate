<?php

namespace ServiceCore\Validate\Validator;

use DateTime;
use Laminas\Validator\Date as DateValidator;

/**
 * In addition to validating that the given value is in the correct DateTime format,
 * also validates that the given value is within 'range' days of the current date
 */
class DateRange extends DateValidator
{
    public const NOT_WITHIN_RANGE = 'notWithinRange';

    protected array $options = [
        'range' => 0
    ];

    private array $newMessageTemplates = [
        self::NOT_WITHIN_RANGE => 'Date must be within %value% day(s) of today'
    ];

    public function __construct($options = [])
    {
        $this->messageTemplates = \array_merge($this->newMessageTemplates, $this->messageTemplates);

        parent::__construct($options);
    }

    public function isValid($value): bool
    {
        $isValid = parent::isValid($value);

        if ($isValid) {
            $isValid = $this->validateRange($value);
        }

        return $isValid;
    }

    public function getRange(): int
    {
        return $this->options['range'];
    }

    public function setRange(int $range): self
    {
        $this->options['range'] = \abs($range);

        return $this;
    }

    private function validateRange($value): bool
    {
        $dateTimeDate = DateTime::createFromFormat($this->getFormat(), $value);
        $diff         = $dateTimeDate->diff(new DateTime(), true);

        if ($diff->days > $this->getRange()) {
            $this->error(self::NOT_WITHIN_RANGE, $this->getRange());

            return false;
        }

        return true;
    }
}
