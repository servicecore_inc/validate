<?php

namespace ServiceCore\Validate\Validator;

use Laminas\Validator\AbstractValidator;

class Decimal extends AbstractValidator
{
    public const ABOVE_PRECISION = 'abovePrecision';
    public const BELOW_PRECISION = 'belowPrecision';
    public const ABOVE_SCALE     = 'aboveScale';
    public const BELOW_SCALE     = 'belowScale';
    public const NOT_NUMERIC     = 'notNumeric';

    /** @var int */
    private $maxPrecision = 8;

    /** @var int */
    private $minPrecision = 1;

    /** @var int */
    private $maxScale = 2;

    /** @var int */
    private $minScale = 0;

    /** @var array */
    protected $messageTemplates = [
        self::ABOVE_PRECISION => 'Value is above the maximum precision defined',
        self::BELOW_PRECISION => 'Value is below the minimum precision defined',
        self::ABOVE_SCALE     => 'Value is above the maximum scale defined',
        self::BELOW_SCALE     => 'Value is below the minimum scale defined',
        self::NOT_NUMERIC     => 'Value is not a number'
    ];

    public function isValid($value): bool
    {
        if (!\is_numeric($value)) {
            $this->error(self::NOT_NUMERIC);

            return false;
        }

        $precision = \strlen($value);
        $scale     = 0;
        if (false !== \strpos($value, '.')) {
            $precision = \strlen(\substr(\strstr($value, '.', true), 0));
            $scale     = \strlen(\substr(\strrchr($value, '.'), 1));
        }

        if ($scale > $this->maxScale) {
            $this->error(self::ABOVE_SCALE);

            return false;
        }

        if ($scale < $this->minScale) {
            $this->error(self::BELOW_SCALE);

            return false;
        }

        if ($precision > $this->maxPrecision) {
            $this->error(self::ABOVE_PRECISION);

            return false;
        }

        if ($precision < $this->minPrecision) {
            $this->error(self::BELOW_PRECISION);

            return false;
        }


        return true;
    }

    public function setMaxPrecision($precision): self
    {
        $this->maxPrecision = $precision;

        return $this;
    }

    public function setMaxScale($scale): self
    {
        $this->maxScale = $scale;

        return $this;
    }

    public function setMinPrecision($precision): self
    {
        $this->minPrecision = $precision;

        return $this;
    }

    public function setMinScale($scale): self
    {
        $this->minScale = $scale;

        return $this;
    }

    public function getMaxPrecision(): int
    {
        return $this->maxPrecision;
    }

    public function getMinPrecision(): int
    {
        return $this->minPrecision;
    }

    public function getMaxScale(): int
    {
        return $this->maxScale;
    }

    public function getMinScale(): int
    {
        return $this->minScale;
    }
}
