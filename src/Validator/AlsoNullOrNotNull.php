<?php

namespace ServiceCore\Validate\Validator;

use Laminas\Validator\AbstractValidator;

class AlsoNullOrNotNull extends AbstractValidator
{
    public const FIELD_NOT_NULL = 'fieldNotNull';
    public const FIELD_NULL     = 'fieldNull';

    public const FIELD_NOT_NULL_VERBIAGE = 'Field \'%value%\' must also be null';
    public const FIELD_NULL_VERBIAGE     = 'Field \'%value%\' must also be not null';

    protected array $messageTemplates = [
        self::FIELD_NOT_NULL => self::FIELD_NOT_NULL_VERBIAGE,
        self::FIELD_NULL     => self::FIELD_NULL_VERBIAGE
    ];

    public function isValid($value, $context = null): bool
    {
        $data  = $context;
        $field = $this->getOption('field');

        if ($value === null) {
            if (\array_key_exists($field, $data) && $data[$field] !== null) {
                $this->error(self::FIELD_NOT_NULL, $field);

                return false;
            }
        } else if ((\array_key_exists($field, $data) && $data[$field] === null)
            || !\array_key_exists($field, $data)
        ) {
            $this->error(self::FIELD_NULL, $field);

            return false;
        }

        return true;
    }
}
