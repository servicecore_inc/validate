<?php

namespace ServiceCore\Validate\Validator\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use RuntimeException;
use ServiceCore\Validate\Validator\AbstractEntityValidator;
use Laminas\ServiceManager\Factory\FactoryInterface;

class EntityValidator implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): AbstractEntityValidator {
        if (!\is_a($requestedName, AbstractEntityValidator::class, true)) {
            throw new RuntimeException(
                \sprintf(
                    'Class %s does not extend %s',
                    $requestedName,
                    AbstractEntityValidator::class
                )
            );
        }

        $entityManager = $container->get(EntityManager::class);

        return new $requestedName($entityManager, $options);
    }
}
