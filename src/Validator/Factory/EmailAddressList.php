<?php

namespace ServiceCore\Validate\Validator\Factory;

use Interop\Container\ContainerInterface;
use ServiceCore\Validate\Validator\EmailAddressList as EmailAddressListValidator;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\Validator\EmailAddress as EmailAddressValidator;
use Laminas\Validator\ValidatorPluginManager;

class EmailAddressList implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): EmailAddressListValidator {
        /** @var ValidatorPluginManager $validatorContainer */
        $validatorContainer = $container->get(ValidatorPluginManager::class);

        /** @var EmailAddressValidator $singleAddressValidator */
        $singleAddressValidator = $validatorContainer->get(EmailAddressValidator::class);

        return new EmailAddressListValidator($singleAddressValidator, $options ?: []);
    }
}
