<?php

namespace ServiceCore\Validate\Validator\Factory;

use Interop\Container\ContainerInterface;
use Phpass\Strength;
use ServiceCore\Validate\Validator\PasswordStrength as PasswordStrengthValidator;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PasswordStrength implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): PasswordStrengthValidator {
        $strengthTester = new Strength();

        return new PasswordStrengthValidator($strengthTester);
    }
}
