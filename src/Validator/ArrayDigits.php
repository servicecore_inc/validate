<?php

namespace ServiceCore\Validate\Validator;

use Laminas\Validator\AbstractValidator;

class ArrayDigits extends AbstractValidator
{
    public const NOT_DIGITS   = 'notDigits';
    public const STRING_EMPTY = 'digitsStringEmpty';
    public const INVALID      = 'digitsInvalid';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $messageTemplates = [
        self::NOT_DIGITS   => 'The input must contain only digits',
        self::STRING_EMPTY => 'The input is an empty string',
        self::INVALID      => 'Invalid type given. String, integer or float expected',
    ];

    public function isValid($values): bool
    {
        if (!\is_array($values)) {
            $values = (array)$values;
        }

        foreach ($values as $value) {
            if (!\is_string($value) && !\is_int($value) && !\is_float($value)) {
                $this->error(self::INVALID);
                return false;
            }

            $this->setValue((string) $value);

            if ('' === $this->getValue()) {
                $this->error(self::STRING_EMPTY);
                return false;
            }

            if (!\is_numeric($value)) {
                $this->error(self::NOT_DIGITS);
                return false;
            }
        }

        return true;
    }
}
