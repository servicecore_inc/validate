<?php

namespace ServiceCore\Validate\Validator;

use Laminas\Validator\AbstractValidator;

class IsNull extends AbstractValidator
{
    public const NOT_NULL          = 'notNull';
    public const NOT_NULL_VERBIAGE = 'Value must be null';

    /** @var array */
    protected $messageTemplates = [
        self::NOT_NULL => self::NOT_NULL_VERBIAGE
    ];

    public function isValid($value): bool
    {
        if ($value !== null) {
            $this->error(self::NOT_NULL);
            return false;
        }

        return true;
    }
}
