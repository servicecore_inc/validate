<?php

namespace ServiceCore\Validate\Validator;

class NotExistsInRepository extends AbstractEntityValidator
{
    public const ITEM_EXISTS   = 'itemExists';
    public const ITEM_IS_ARRAY = 'itemIsArray';

    /** @var array */
    protected $messageTemplates = [
        self::ITEM_EXISTS   => 'The item %value% already exists',
        self::ITEM_IS_ARRAY => 'The value passed in must not be an array'
    ];

    public function isValid($value): bool
    {
        if (\is_array($value)) {
            $this->error(self::ITEM_IS_ARRAY);

            return false;
        }

        $entity = $this->findEntity($value);

        if ($entity !== null && \count($entity) > 0) {
            $this->error(self::ITEM_EXISTS);

            return false;
        }

        return true;
    }
}
