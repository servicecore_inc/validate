<?php

namespace ServiceCore\Validate\Validator;

use Laminas\Validator\AbstractValidator;

class OneOrOtherNull extends AbstractValidator
{
    public const FIELD_NOT_NULL = 'fieldNotNull';

    public const FIELD_NOT_NULL_VERBIAGE = 'For this value to be set, field \'%value%\' must be null';

    protected array $messageTemplates = [
        self::FIELD_NOT_NULL => self::FIELD_NOT_NULL_VERBIAGE
    ];

    public function isValid($value, $context = null): bool
    {
        $data  = $context;
        $field = $this->getOption('field');

        if (\array_key_exists($field, $data) && $data[$field] !== null && $value !== null) {
            $this->error(self::FIELD_NOT_NULL, $field);

            return false;
        }

        return true;
    }
}
