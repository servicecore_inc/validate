<?php

namespace ServiceCore\Validate\Constraint;

use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractConstraint
{
    private EntityManagerInterface $entityManager;

    abstract public function constrain();

    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    public function setEntityManager(EntityManagerInterface $entityManager): self
    {
        $this->entityManager = $entityManager;

        return $this;
    }
}
