<?php

namespace ServiceCore\Validate\Constraint;

use Doctrine\ORM\EntityManagerInterface;
use Laminas\Router\RouteMatch;

class LimitByRouteEntity extends AbstractConstraint
{
    private RouteMatch $routeMatch;
    private string     $entity;
    private string     $routeParam;

    public function __construct(EntityManagerInterface $em, RouteMatch $routeMatch, string $entity, string $routeParam)
    {
        $this->entity     = $entity;
        $this->routeParam = $routeParam;
        $this->routeMatch = $routeMatch;

        $this->setEntityManager($em);
    }

    public function constrain(): ?object
    {
        if (!$id = $this->routeMatch->getParam($this->routeParam)) {
            return null;
        }

        return $this->getEntityManager()->getReference($this->entity, $id);
    }
}
