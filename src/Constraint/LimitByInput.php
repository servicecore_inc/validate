<?php

namespace ServiceCore\Validate\Constraint;

class LimitByInput extends AbstractConstraint
{
    private array $inputParams;

    /** @var mixed */
    private $value;

    public function __construct(array $inputParams, $value)
    {
        $this->inputParams = $inputParams;
        $this->value       = $value;
    }

    public function constrain()
    {
        return $this->inputParams[$this->value] ?? null;
    }
}
