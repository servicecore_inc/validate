<?php

namespace ServiceCore\Validate\Constraint;

class LimitByValue extends AbstractConstraint
{
    /** @var mixed */
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function constrain()
    {
        return $this->value;
    }
}
