<?php

namespace ServiceCore\Validate\Constraint\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Validate\Constraint\LimitByRouteEntity as Constraint;

class LimitByAssociation implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): Constraint
    {
        $routeMatch = $container->get('Application')->getMvcEvent()->getRouteMatch();
        $em         = $container->get(EntityManager::class);

        return new Constraint($em, $routeMatch, $options['entity'], $options['getter']);
    }
}
