<?php

namespace ServiceCore\Validate\Constraint\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ApiTools\ContentNegotiation\ParameterDataContainer;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Validate\Constraint\LimitByInput as Constraint;

class LimitByInput implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): Constraint
    {
        /** @var ParameterDataContainer $app */
        $params = $container->get('Application')->getMvcEvent()->getParam('LaminasContentNegotiationParameterData');

        return new Constraint($params->getBodyParams(), $options['field']);
    }
}
