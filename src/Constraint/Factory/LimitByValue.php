<?php

namespace ServiceCore\Validate\Constraint\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Validate\Constraint\LimitByValue as Constraint;

class LimitByValue implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): Constraint
    {
        return new Constraint($options['value']);
    }
}
