<?php

namespace ServiceCore\Validate\Constraint;

use Doctrine\ORM\EntityManagerInterface;
use Laminas\Router\RouteMatch;

class LimitByParam extends AbstractConstraint
{
    private RouteMatch $routeMatch;
    private string     $className;
    private string     $param;

    public function __construct(EntityManagerInterface $em, RouteMatch $routeMatch, string $className, string $param)
    {
        $this->routeMatch = $routeMatch;
        $this->className  = $className;
        $this->param      = $param;

        $this->setEntityManager($em);
    }

    public function constrain(): ?object
    {
        return $this->getEntityManager()->getReference(
            $this->className,
            $this->routeMatch->getParams()[$this->param]
        );
    }
}
