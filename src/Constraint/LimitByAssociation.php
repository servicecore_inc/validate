<?php

namespace ServiceCore\Validate\Constraint;

class LimitByAssociation extends AbstractConstraint
{
    private string $entity;
    private string $getter;

    public function __construct(string $entity, string $getter)
    {
        $this->entity = $entity;
        $this->getter = $getter;
    }

    public function constrain(): ?object
    {
        if (!\method_exists($this->entity, $this->getter)) {
            throw new \InvalidArgumentException(
                \sprintf(
                    'Method %s does not exist in %s',
                    $this->getter,
                    \get_class($this->entity)
                )
            );
        }

        return $this->entity->{$this->getter};
    }
}
